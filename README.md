- Control Volume
```bash
./setvol.sh up # increase volume of default sink
./setvol.sh down # decrease volume of default sink
./setvol.sh <setting> # set volume of default sink; refer to pactl docs
```
