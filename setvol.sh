#!/bin/sh

EC_NOSINK=10
EC_NOVOLSET=11

sink=$(pactl get-default-sink)

if [[ -z "$sink" ]]; then
    echo "could not determine default sink; exiting..."
    exit $EC_NOSINK
fi

setting="$1"

if [[ -z "$setting" ]]; then
    echo "volume setting was not given; exiting..."
    exit $EC_NOVOLSET
elif [[ "$setting" = "up" ]]; then
    setting="+5%"
elif [[ "$setting" = "down" ]]; then
    setting="-5%"
fi

pactl set-sink-volume "$sink" "$setting"
